# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from decimal import Decimal
from dateutil.relativedelta import relativedelta
from collections import defaultdict
import math
from trytond.tools import reduce_ids, grouped_slice
from trytond.model.exceptions import AccessError
from trytond.i18n import gettext
from trytond.model import (
    ModelView, ModelSQL, fields, Workflow, Unique, DeactivableMixin)
from trytond.transaction import Transaction
from trytond.pool import Pool
from trytond import backend
from trytond.pyson import Eval, If
from trytond.modules.company.model import (
    employee_field, set_employee, reset_employee)
from .conversion_rate import ConversionRate


class CreditLine(DeactivableMixin, ModelSQL, ModelView):
    'Credit Line'
    __name__ = 'credit.line'
    
    name = fields.Char('Credit Line', help='Type product or credit line')
    company = fields.Many2One('company.company', 'Company')
    type_loan = fields.Selection([
        ('', ''),
        ('microcredit', 'microcredit'),
        ('consumer_loan', 'Consumer Loan'),
        ('comercial_loan', 'Comercial Loan')
    ], 'Type Loan', required=True)
    annual_effective_rate = fields.Numeric('Annual Effective Rate', digits=(16, 2), help='Annual Effective Rate')
    annual_nominal_rate = fields.Function(fields.Numeric(
            "Annual Nominal Rate", digits=(16, 2)),
        'on_change_with_annual_nominal_rate', setter='set_annual_nominal_rate')
    minimun_amount = fields.Numeric('Minimun Amount', digits=(16,2), required=True, help='Minimum loan amount')
    maximun_amount = fields.Numeric('Maximun Amount', digits=(16,2), required=True, help='Maximum loan amount')
    minimun_payment_term = fields.Float('Minimun Payment Term', required=True)
    maximun_payment_term = fields.Float('Maximun Payment Term', required=True)

    @fields.depends('annual_effective_rate')
    def on_change_with_annual_nominal_rate(self, name=None):
        if self.annual_effective_rate is None:
            return
        rate = ConversionRate.tea_to_tna(self.annual_effective_rate)
        rate = rate.quantize(
            Decimal(1) / 10 ** self.__class__.annual_nominal_rate.digits[1])
        return rate

    @fields.depends('annual_effective_rate', 'annual_nominal_rate')
    def on_change_annual_nominal_rate(self):
        if self.annual_nominal_rate is not None:
            rate = ConversionRate.tna_to_tea(self.annual_nominal_rate)
            self.annual_effective_rate = rate.quantize(
                Decimal(1) / 10 ** self.__class__.annual_effective_rate.digits[1])

    @classmethod
    def set_annual_nominal_rate(cls, records, name, value):
        pass
    
    @staticmethod
    def default_company():
        return Transaction().context.get('company')