# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from datetime import date
from decimal import Decimal

from trytond.i18n import gettext
from trytond.model import ModelSQL, ModelView, Workflow, fields
from trytond.model.exceptions import AccessError
from trytond.modules.company.model import employee_field, reset_employee, set_employee
from trytond.pool import Pool
from trytond.pyson import Bool, Eval, If
from trytond.transaction import Transaction

from .conversion_rate import ConversionRate
from .exceptions import (
    LoanMissingSequence,
)

STATES = {'readonly': (Eval('state') != 'draft')}
_ZERO = Decimal('0.0')


class Loan(Workflow, ModelSQL, ModelView):
    "Loan"
    __name__ = 'loan'
    _rec_name = 'number'

    number = fields.Char('Number', readonly=True, help="Secuence",
                         )
    family_reference = fields.Char('Family_Reference')
    friend_reference = fields.Char('Friend_Reference')
    phone_family = fields.Char('Phone_Family')
    phone_friend = fields.Char('Phone_Friend')
    party = fields.Many2One('party.party', 'Party',
            states=STATES, required=True, depends=['state'])
    date_effective = fields.Date('Date Effective', states=STATES,
                                 required=True)
    state = fields.Selection([
            ('', ''),
            ('draft', 'Draft'),
            ('approved', 'Approved'),
            ('processed', 'Processed'),
            # ('posted', 'Posted'),
            ('cancelled', 'Cancel'),
            ('paid', 'Paid'),
            ], 'State', readonly=True)

    company = fields.Many2One('company.company', 'Company', required=True,
                              states={
                                  'readonly': (Eval('state') != 'draft') | Eval('lines', [0]),
                                  },
                              domain=[
                                  ('id', If(Eval('context', {}).contains('company'), '=', '!='),
                                   Eval('context', {}).get('company', 0)),
                                  ],
                              depends=['state'])
    description = fields.Char('Description')
    amount = fields.Numeric('Total Amount', digits=(16, 2),
        states={
            'readonly': Eval('state') != 'draft',
        })
    credit_line = fields.Many2One('credit.line', 'Credit Line', required=True,
        states={
            'readonly': Eval('state') != 'draft',
        })
    payment_term = fields.Many2One('loan.payment_term',
        'Payment Term', states={
            'readonly': Eval('state') != 'draft',
        }, depends=['state'],
        )
    approved_by = employee_field(
        "Approved By",
        states=['processed', 'done', 'cancelled'])
    processed_by = employee_field(
        "Processed By",
        states=['processed', 'done', 'cancelled'])
    comment = fields.Text('Comment')

    currency = fields.Many2One('currency.currency', 'Currency', required=True,
        states={
            'readonly': Eval('state') != 'draft',
        })
    guarantors = fields.One2Many('loan.guarantor', 'loan', string='Guarantors',
        states={
            'readonly': Eval('state') != 'draft',
        })
    annual_effective_rate = fields.Numeric('Annual Effective Rate', digits=(16, 2),
        states={
            'readonly': Eval('state') != 'draft',
        })
    disbursement = fields.Many2One('loan.disbursement', 'Disbursement', readonly=True)
    amount_fee = fields.Function(fields.Numeric('Amount Fee', digits=(16, 2)), 'get_amount_fee')
    payday = fields.Integer('Payday', states={
        'required': True,
        'readonly': (Eval('state') != 'draft'),
        }, help='Day within the month on which the customer pays the loan installment')
    reference = fields.Char('Reference', states={
        'required': Bool(Eval('credit_migrated')),
        'invisible': ~Bool(Eval('credit_migrated')),
        'readonly': (Eval('state') != 'draft'),
        })
    next_fee = fields.Integer('Next Fee', states={
        'required': Bool(Eval('credit_migrated')),
        'invisible': ~Bool(Eval('credit_migrated')),
        'readonly': (Eval('state') != 'draft'),
        })
    historical_balance = fields.Numeric('Historical Balance', states={
        'required': Bool(Eval('credit_migrated')),
        'invisible': ~Bool(Eval('credit_migrated')),
        'readonly': (Eval('state') != 'draft'),
        })
    credit_migrated = fields.Boolean('Credit Migrated')
    date_migrated = fields.Date('Date Migrated', states={
        'required': Bool(Eval('credit_migrated')),
        'invisible': ~Bool(Eval('credit_migrated')),
        'readonly': (Eval('state') != 'draft'),
        })
    line_move = fields.Many2One('account.move.line', 'Line Move',
        domain=[
            ('party', '=', Eval('party')),
            ('reconciliation', '=', None),
            ],
        states={
            'required': Bool(Eval('credit_migrated')),
            'invisible': ~Bool(Eval('credit_migrated')),
            'readonly': (Eval('state') != 'draft'),
        })

    @classmethod
    def __setup__(cls):
        super(Loan, cls).__setup__()
        cls._order.insert(0, ('date_effective', 'ASC'))
        cls._transitions |= set((
                ('draft', 'approved'),
                ('approved', 'processed'),
                ('processed', 'done'),
                ('processed', 'draft'),
                ('draft', 'cancelled'),
                ('approved', 'cancelled'),
                ('approved', 'draft'),
                ('cancelled', 'draft'),
                ))
        cls._buttons.update({
                'cancel': {
                    'invisible': ~Eval('state').in_(['draft', 'done']),
                    'depends': ['state'],
                    },
                'draft': {
                    'invisible': ~Eval('state').in_(
                        ['cancelled', 'approved']),
                    'icon': If(Eval('state') == 'cancelled',
                        'tryton-undo',
                        'tryton-back'),
                    'depends': ['state'],
                    },
                'approve': {
                    'invisible': Eval('state') != 'draft',
                    'depends': ['state'],
                    },
                'process': {
                    'invisible': Eval('state') != 'approved',
                    'icon': 'tryton-forward',
                    'depends': ['state'],
                    },
                # 'calculate': {
                #     'invisible': Eval('state') != 'draft',
                #     'icon': 'tryton-refresh',
                #     'depends': ['state'],
                #     },
                })

    @staticmethod
    def default_currency():
        pool = Pool()
        Company = pool.get('company.company')
        company = Transaction().context.get('company')
        if company:
            return Company(company).currency.id

    @staticmethod
    def default_state():
        return 'draft'

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @fields.depends('credit_line', 'annual_effective_rate')
    def on_change_credit_line(self):
        if self.credit_line:
            rate = Decimal(self.credit_line.annual_effective_rate)
            self.annual_effective_rate = rate.quantize(
                Decimal(1) / 10 ** self.__class__.annual_effective_rate.digits[1])

    @classmethod
    @ModelView.button
    @Workflow.transition('cancelled')
    def cancel(cls, loans):
        pass

    # @classmethod
    # def validate_loan(cls, loans):
    #     lines = []
    #     for loan in loans:
    #         lines.extend(loan.lines)
    #     pool = Pool()
    #     LoanLine = pool.get('loan.line')
    #     LoanLine.validate_line(lines)

    @classmethod
    @ModelView.button
    @Workflow.transition('draft')
    @reset_employee('approved_by', 'processed_by')
    def draft(cls, loans):
        pass

    @classmethod
    @ModelView.button
    @Workflow.transition('approved')
    @set_employee('approved_by')
    def approve(cls, loans):
        for loan in loans:
            if not loan.check_loan_approve():
                raise AccessError(gettext('loan.msg_error_loan_payment_plan'))
            if loan.credit_migrated:
                line_move = loan.line_move
                amount = line_move.debit - line_move.credit
                if amount != loan.historical_balance:
                    raise AccessError(gettext('loan.msg_amount_diferent_historical_balance'))
                amount_fee = loan.get_amount_fee()
                amount_validate = (len(loan.payment_term.lines) - loan.next_fee + 1) * amount_fee
                if amount_validate < loan.historical_balance or amount_validate - loan.historical_balance > amount_fee:
                    raise AccessError(gettext('loan.msg_next_fee_incorrect'))

            if not loan.number:
                loan.set_number()

    @classmethod
    @ModelView.button
    @Workflow.transition('processed')
    @set_employee('processed_by')
    def process(cls, loans):
        cls.create_disbursement(loans)

    @fields.depends('payday', 'date_effective')
    def on_change_payday(self, name=None):
        if self.payday and self.date_effective:
            try:
                date(self.date_effective.year, self.date_effective.month, self.payday)
            except:
                raise AccessError(gettext('loan.msg_error_payday'))

    @classmethod
    def create_disbursement(cls, loans):
        pool = Pool()
        Disbursement = pool.get('loan.disbursement')
        for loan in loans:
            if loan.disbursement:
                continue
            disbursement = loan.get_disbursement()
            Disbursement.save([disbursement])
            if loan.state != 'processed':
                loan.state = 'processed'
            loan.disbursement = disbursement
            loan.save()

    def get_amount_fee(self, name=None):
        if self.annual_effective_rate is not None and self.amount and self.payment_term:
            tem = 0
            if self.annual_effective_rate:
                tem = ConversionRate.tea_to_tem(self.annual_effective_rate).quantize(Decimal(1) / 10 ** 4)
                amount_due = ConversionRate.get_amount_due(tem, self.amount, len(self.payment_term.lines))
            else:
                amount_due = 0
                for date, amount_ in self.payment_term.compute(
                    self.amount, self.currency, self.date_effective):
                    amount_due = amount_
                    break
            return amount_due.quantize(Decimal(1) / 10 ** 2)

    def check_loan_approve(self):
        num_lines = len(self.payment_term.lines)
        credit_l = self.credit_line
        amount, line = False, False
        if self.amount >= credit_l.minimun_amount and self.amount <= credit_l.maximun_amount:
            amount = True
        if num_lines >= credit_l.minimun_payment_term and num_lines <= credit_l.maximun_payment_term:
            line = True
        return amount and line
        # amount = self.amount
        # amount_lines = sum([line.amount for line in self.lines])
        # if self.company.currency.is_zero(amount - amount_lines):
        #     return True
        # return False

    def get_disbursement(self):
        pool = Pool()
        Disbursement = pool.get('loan.disbursement')
        # Date = pool.get('ir.date')
        disbursement = Disbursement()
        disbursement.party = self.party.id
        disbursement.company = self.company.id
        disbursement.currency = self.currency.id
        disbursement.party = self.party.id
        disbursement.amount = self.amount
        disbursement.date_effective = self.date_effective
        disbursement.payment_term = self.payment_term
        disbursement.company = self.company
        disbursement.loan = self
        disbursement.state = 'draft'
        disbursement.payday = self.payday
        return disbursement

    def set_number(self):
        pool = Pool()
        Configuration = pool.get('loan_customer.configuration')
        configuration = Configuration(1)
        company = Transaction().context.get('company')
        sequence = configuration.get_multivalue('loan_sequence',
                    company=company)
        if not sequence:
            raise LoanMissingSequence(gettext('loan.msg_sequence_missing'))
        seq = sequence.get()
        self.write([self], {'number': seq})

    # @classmethod
    # def get_amount_to_pay(cls, loans, name):
    #     pool = Pool()
    #     Currency = pool.get('currency.currency')
    #     # Date = pool.get('ir.date')

    #     # today = Date.today()
    #     res = dict((x.id, Decimal(0)) for x in loans)
    #     for loan in loans:
    #         if loan.state != 'posted':
    #             continue
    #         amount = Decimal(0)
    #         # amount_currency = Decimal(0)
    #         for line in loan.lines_to_pay:
    #             if line.reconciliation:
    #                 continue
    #             # if (name == 'amount_to_pay_today'
    #             #         and (not line.maturity_date
    #             #             or line.maturity_date > today)):
    #             #     continue
    #             # else:
    #             amount += line.debit - line.credit
    #         for line in loan.payment_lines:
    #             if line.reconciliation:
    #                 continue
    #             # if (line.second_currency
    #             #         and line.second_currency == loan.currency):
    #             #     amount_currency += line.amount_second_currency
    #             # else:
    #             amount += line.debit - line.credit
    #         if amount != Decimal(0):
    #             pass
    #             # with Transaction().set_context(date=loan.currency_date):
    #             #     amount_currency += Currency.compute(
    #             #         loan.company.currency, amount, loan.currency)
    #         res[loan.id] = amount
    #     return res


# class LoanLine(ModelSQL, ModelView):
#     'Loan Line'
#     __name__ = 'loan.line'

#     loan = fields.Many2One('loan', 'Loan',
#                               ondelete='CASCADE', required=True)
#     amount = fields.Numeric('Total Amount', digits=(16, 2))
#     maturity_date = fields.Date('Maturity Date',
#                                  required=True)
#     state = fields.Selection([
#                 ('draft', 'Draft'),
#                 ('pending', 'Pending'),
#                 ('partial', 'Partial'),
#                 ('paid', 'Paid'),
#             ], 'State', states={
#                 'required': True,
#             }, depends=['origin'])
#     description = fields.Char('Description')
#     currency = fields.Function(fields.Many2One(
#             'currency.currency', "Currency"), 'on_change_with_currency')

#     origin = fields.Reference("Origin", selection='get_origin')

#     @classmethod
#     def __setup__(cls):
#         super(LoanLine, cls).__setup__()
#         cls._order.insert(0, ('maturity_date', 'ASC'))

#     @classmethod
#     def default_state():
#         return 'draft'

#     @fields.depends('loan', '_parent_loan.currency')
#     def on_change_with_currency(self, name=None):
#         if self.loan:
#             return self.loan.currency.id


#     @classmethod
#     def _get_origin(cls):
#         'Return list of Model names for origin Reference'
#         return ['loan.line']

#     @classmethod
#     def get_origin(cls):
#         Model = Pool().get('ir.model')
#         get_name = Model.get_name
#         models = cls._get_origin()
#         return [(None, '')] + [(m, get_name(m)) for m in models]

    # @classmethod
    # def write(cls, *args):
    #     super(LoanLine, cls).write(*args)
    #     actions = iter(args)
    #     all_lines = []
    #     for lines, values in zip(actions, actions):
    #         all_lines.extend(lines)
    #     cls.validate_line(all_lines)

    # @classmethod
    # def validate_line(cls, lines):
    #     line = cls.__table__()
    #     cursor = Transaction().connection.cursor()
    #     pending_lines = []
    #     paid_lines = []
    #     for m in lines:
    #         if not m.origin and m.state == 'paid':
    #             pending_lines.append(m.id)
    #         elif m.origin and m.state in ('pending', 'partial'):
    #             paid_lines.append(m.id)
    #     for move_ids, state in (
    #             (pending_lines, 'pending'),
    #             (paid_lines, 'paid'),
    #             ):
    #         if move_ids:
    #             for sub_ids in grouped_slice(move_ids):
    #                 red_sql = reduce_ids(line.id, sub_ids)
    #                 # Use SQL to prevent double validate loop
    #                 cursor.execute(*line.update(
    #                         columns=[line.state],
    #                         values=[state],
    #                         where=red_sql))


class LoanGuarantor(ModelSQL, ModelView):
    "Loan Guarantor"
    __name__ = 'loan.guarantor'

    party = fields.Many2One('party.party', 'Party', required=True)
    loan = fields.Many2One('loan', 'Loan', required=True)
    type_guarantor = fields.Selection([
        ('', ''),
        ('co-sign', 'Co-sign'),
        ('debtor', 'Deptor'),
        ('mortgage', 'Mortgage'),
    ], 'Type Guarantor')
    amount_available = fields.Numeric('Amount Available', required=True)
    description = fields.Char('Description')
    number_deet = fields.Char('Number Deet', states={
            'required': Eval('type_guarantor') == 'mortgage',
            'invisible': Eval('type_guarantor') != 'mortgage',
        })
    date_of_deet = fields.Date('Date of Deet', states={
            'required': Eval('type_guarantor') == 'mortgage',
            'invisible': Eval('type_guarantor') != 'mortgage',
        })
    family_reference = fields.Char('Family_Reference')
    friend_reference = fields.Char('Friend_Reference')
    phone_family = fields.Char('Phone_Family')
    phone_friend = fields.Char('Phone_Friend')
