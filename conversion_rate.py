from decimal import Decimal

class ConversionRate:

    def __init__(self):
        pass

    @classmethod
    def tea_to_tem(cls, percent):
        return ((1+percent/100)**Decimal(1/12))-1

    @classmethod
    def tna_to_tea(cls, percent):
        return ((1+ (percent/100/12))**12-1)*100

    @classmethod    
    def tea_to_tna(cls, percent):
        return ((1+ percent/100)**Decimal(1/12)-1) * 12 * 100

    @classmethod
    def get_amount_due2(cls, rate, capital, terms):
        return capital*(((1-(1+rate)**-terms)/rate)**-1)

    @classmethod
    def get_amount_due(cls, rate, capital, terms):
        return (rate * capital)/(1-((1+rate)**-terms))