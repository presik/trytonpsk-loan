# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from decimal import Decimal
from trytond.model import ModelView, ModelSQL, fields, ModelSingleton
from trytond.transaction import Transaction
from trytond.pyson import Eval, Id
from trytond.pool import PoolMeta, Pool
from trytond.modules.company.model import (
    CompanyMultiValueMixin, CompanyValueMixin)
from .conversion_rate import ConversionRate


class Configuration(
        ModelSingleton, ModelSQL, ModelView, CompanyMultiValueMixin):
    'Loan Customer Configuration'
    __name__ = 'loan_customer.configuration'

    require_invoice = fields.Boolean("Require Invoice")
    default_account_debit = fields.MultiValue(fields.Many2One(
            'account.account', "Default Account Debit",
            domain=[
                ('type', '!=', None),
                ('closed', '!=', True),
                ('company', '=', Eval('context', {}).get('company', -1)),
                ]))
    default_account_interest = fields.MultiValue(fields.Many2One(
        'account.account', "Default Account Interest",
        domain=[
            ('type', '!=', None),
            ('closed', '!=', True),
            ('company', '=', Eval('context', {}).get('company', -1)),
            ]))
    default_account_interest_delay = fields.MultiValue(fields.Many2One(
        'account.account', "Default Account Interest Delay",
        domain=[
            ('type', '!=', None),
            ('closed', '!=', True),
            ('company', '=', Eval('context', {}).get('company', -1)),
            ]))
    default_account_fees = fields.MultiValue(fields.Many2One(
        'account.account', "Default Account Fees",
        domain=[
            ('type', '!=', None),
            ('closed', '!=', True),
            ('company', '=', Eval('context', {}).get('company', -1)),
            ]))
    loan_sequence = fields.MultiValue(fields.Many2One(
            'ir.sequence', "Loan Sequence", required=True,
            domain=[
                ('company', 'in',
                    [Eval('context', {}).get('company', -1), None]),
                ('sequence_type', '=',
                    Id('loan',
                        'sequence_type_loan')),
                ]))
    disbursement_sequence = fields.MultiValue(fields.Many2One(
            'ir.sequence', "Disbursement Sequence", required=True,
            domain=[
                ('company', 'in',
                    [Eval('context', {}).get('company', -1), None]),
                ('sequence_type', '=',
                    Id('loan',
                        'sequence_type_loan')),
                ]))

    @classmethod
    def multivalue_model(cls, field):
        pool = Pool()
        if field in ('loan_sequence', 'disbursement_sequence'):
            return pool.get('loan_customer.configuration.sequence')
        accounts = (
            'default_account_debit',
            'default_account_interest',
            'default_account_interest_delay',
            'default_account_fees'
            )
        if field in accounts:
            return pool.get('loan_customer.configuration.default_account')
        
        return super(Configuration, cls).multivalue_model(field)


class ConfigurationDefaultAccount(ModelSQL, CompanyValueMixin):
    "Loan Customer Configuration Default Account"
    __name__ = 'loan_customer.configuration.default_account'
    default_account_debit = fields.Many2One(
        'account.account', "Default Account Debit",
        domain=[
            ('type', '!=', None),
            ('closed', '!=', True),
            ('company', '=', Eval('company', -1)),
            ],
        depends=['company'])
    default_account_interest = fields.Many2One(
        'account.account', "Default Account Interest",
        domain=[
            ('type', '!=', None),
            ('closed', '!=', True),
            ('company', '=', Eval('company', -1)),
            ],
        depends=['company'])
    default_account_interest_delay = fields.Many2One(
        'account.account', "Default Account Interest Delay",
        domain=[
            ('type', '!=', None),
            ('closed', '!=', True),
            ('company', '=', Eval('company', -1)),
            ],
        depends=['company'])
    default_account_fees = fields.Many2One(
        'account.account', "Default Account Fees",
        domain=[
            ('type', '!=', None),
            ('closed', '!=', True),
            ('company', '=', Eval('company', -1)),
            ],
        depends=['company'])


class Sequence(ModelSQL, CompanyValueMixin):
    "Loan Customer Configuration Sequence"
    __name__ = 'loan_customer.configuration.sequence'

    loan_sequence = fields.Many2One(
        'ir.sequence', "Loan Sequence",
        domain=[
            ('company', '=', Eval('company', -1)),
            ('sequence_type', '=',
                Id('loan', 'sequence_type_loan')),
            ],
        depends=['company'])
    disbursement_sequence = fields.Many2One(
        'ir.sequence', "Disbursement Sequence",
        domain=[
            ('company', '=', Eval('company', -1)),
            ('sequence_type', '=',
                Id('loan', 'sequence_type_loan')),
            ],
        depends=['company'])


class HistoryRate(ModelSQL, ModelView):
    "History Rate"

    __name__ = 'history.rate'
    type_loan = fields.Selection([
        ('', ''),
        ('microcredit', 'microcredit'),
        ('consumer_loan', 'Consumer Loan'),
        ('comercial_loan', 'Comercial Loan')
    ], 'Type Loan', required=True)
    start_date = fields.Date('Start Date', required=True)
    end_date = fields.Date('End Date', required=True)
    annual_effective_rate = fields.Numeric('Annual Effective Rate', digits=(16, 2), help='Annual Effective Rate')
    month_effective_rate = fields.Function(fields.Numeric(
            "Month Effective Rate", digits=(16, 2)),
        'on_change_with_effective_rate')

    @classmethod
    def __setup__(cls):
        super(HistoryRate, cls).__setup__()
        cls._order = [
            ('type_loan', 'ASC NULLS LAST'),
            ('start_date', 'ASC'),
            ('id', 'ASC'),
            ]

    @fields.depends('annual_effective_rate')
    def on_change_with_effective_rate(self, name=None):
        if self.annual_effective_rate is None:
            return
        rate = ConversionRate.tea_to_tem(self.annual_effective_rate)*100
        rate = rate.quantize(
            Decimal(1) / 10 ** self.__class__.month_effective_rate.digits[1])
        return rate