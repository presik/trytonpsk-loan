from trytond.pool import PoolMeta


class Collection(metaclass=PoolMeta):
    'Collection'
    __name__ = 'collection.collection'

    @classmethod
    def __setup__(cls):
        super(Collection, cls).__setup__()
        cls.origin.selection.extend([('loan.disbursement', 'loan.disbursement')])
