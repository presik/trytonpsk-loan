# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from trytond.pool import Pool
from . import payment_term
from . import loan
from . import configuration
from . import account
from . import disbursement
from . import credit_line
from . import collection


def register():
    Pool.register(
        configuration.Configuration,
        configuration.ConfigurationDefaultAccount,
        configuration.Sequence,
        configuration.HistoryRate,
        payment_term.PaymentTermLoan,
        payment_term.PaymentTermLoanLine,
        payment_term.PaymentTermLoanLineRelativeDelta,
        payment_term.TestPaymentTermView,
        payment_term.TestPaymentTermViewResult,
        account.Move,
        account.MoveLine,
        loan.Loan,
        # loan.LoanLine,
        disbursement.LoanPaymentLine,
        disbursement.LoanDisbursement,
        disbursement.DisbursementLine,
        disbursement.PayLoanStart,
        credit_line.CreditLine,
        loan.LoanGuarantor,
        disbursement.LoanDetailedStart,
        # collection.Collection,
        module='loan', type_='model')
    Pool.register(
        disbursement.DisbursementReport,
        disbursement.ReportLoanDetailed,
        module='loan', type_='report')
    Pool.register(
        disbursement.LoanDetailedWizard,
        disbursement.PayLoan,
        # payment_term.TestPaymentTerm,
        module='loan', type_='wizard')
