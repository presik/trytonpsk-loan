# This file is part of Tryton.  The COPYRIGHT file at the top level of
# this repository contains the full copyright notices and license terms.
from collections import defaultdict
from datetime import date
from decimal import Decimal

from trytond.i18n import gettext
from trytond.model import ModelSQL, ModelView, Workflow, fields
from trytond.model.exceptions import AccessError
from trytond.modules.company import CompanyReport
from trytond.modules.company.model import employee_field, set_employee
from trytond.pool import Pool
from trytond.pyson import Eval, If
from trytond.report import Report
from trytond.tools import grouped_slice, reduce_ids
from trytond.transaction import Transaction
from trytond.wizard import Button, StateReport, StateTransition, StateView, Wizard

from .conversion_rate import ConversionRate
from .exceptions import LoanMissingSequence

STATES = {'readonly': (Eval('state') != 'draft')}
_ZERO = Decimal('0.0')


def rvalue(n):
    return Decimal(str(round(n, 2)))


class LoanDisbursement(Workflow, ModelSQL, ModelView):
    "Loan Disbursement"

    __name__ = 'loan.disbursement'
    _rec_name = 'number'

    number = fields.Char('Number', readonly=True, help="Sequence",
        )
    party = fields.Many2One('party.party', 'Party',
        states=STATES, required=True, depends=['state'])
    date_effective = fields.Date('Date Effective', states=STATES,
        required=True)
    payday = fields.Integer('Payday', states={
        'required': True,
        'readonly': Eval('state') != 'draft',
        })
    state = fields.Selection([
        ('', ''),
        ('draft', 'Draft'),
        ('processed', 'Processed'),
        ('posted', 'Posted'),
        ('cancelled', 'Cancel'),
        ('paid', 'Paid'),
    ], 'State', readonly=True)

    company = fields.Many2One('company.company', 'Company', required=True,
        states={
                'readonly': (Eval('state') != 'draft') | Eval('lines', [0]),
                },
        domain=[
                ('id', If(Eval('context', {}).contains('company'), '=', '!='),
                Eval('context', {}).get('company', 0)),
                ],
        depends=['state'])
    description = fields.Char('Description')
    lines = fields.One2Many('loan_disbursement.line', 'disbursement', 'Lines')
    amount = fields.Numeric('Total Amount', digits=(16, 2), states={
        'required': True,
        'readonly': Eval('state') != 'draft',
        })
    payment_term = fields.Many2One('loan.payment_term',
        'Payment Term', states=STATES, depends=['state'])
    process_by = employee_field(
        "Processed By",
        states=['paid', 'cancelled'])
    comment = fields.Text('Comment')
    currency = fields.Many2One('currency.currency', 'Currency', required=True)
    account_move = fields.Many2One('account.move', 'Account Move', readonly=True)
    loan = fields.Many2One('loan', 'Loan Review', readonly=True)
    account_debit = fields.Many2One('account.account', 'Account',
        states={
            'required': Eval('state').in_(['processed', 'posted']),
            'readonly': Eval('state') != 'draft',
        },
        domain=[
            ('closed', '!=', True),
            ('company', '=', Eval('company', -1)),
            ('type', '!=', None),
            ])

    reconciled = fields.Function(fields.Date('Reconciled',
        states={
            'invisible': ~Eval('reconciled'),
            }),
        'get_reconciled')
    payment_mode = fields.Many2One('account.voucher.paymode', 'Payment Mode',
        states={
            'required': Eval('state') != 'draft',
            'readonly': Eval('state') == 'posted',
        })

    processed_by = employee_field(
        "Processed By",
        states=['processed', 'done', 'cancelled'])

    lines_to_pay = fields.Function(fields.Many2Many(
            'account.move.line', None, None, 'Lines to Pay'),
        'get_lines_to_pay')

    pending_amount = fields.Function(fields.Numeric('Pending Amount',
        digits=(16, 2)), 'get_value_field')

    due_date = fields.Function(fields.Date('Due Date'), 'get_value_field')
    closed_date = fields.Date('Closed Date')
    due_days = fields.Function(fields.Integer('Due Days'),
        'get_value_field')
    fee_paid = fields.Function(fields.Integer('Fee Paid'),
        'get_value_field')
    invoice = fields.Many2One('account.invoice', 'Invoice',
        domain=[
            ('party', '=', Eval('party')),
            ('state', '=', 'posted'),
            ],
        states={
            'readonly': Eval('state') == 'posted',
        })
    principal_balance = fields.Function(fields.Numeric('Principal Balance'), 'get_principal_balance')

    @classmethod
    def __setup__(cls):
        super(LoanDisbursement, cls).__setup__()
        cls._order.insert(0, ('date_effective', 'ASC'))
        cls._transitions |= set((
            ('draft', 'processed'),
            ('processed', 'posted'),
            ('processed', 'draft'),
            ('draft', 'cancelled'),
            ('cancelled', 'draft'),
            ('posted', 'paid'),
            ('paid', 'posted'),
            ))
        cls._buttons.update({
            'cancel': {
                'invisible': ~Eval('state').in_(['draft', 'done']),
                'depends': ['state'],
                },
            'draft': {
                'invisible': ~Eval('state').in_(
                    ['cancelled', 'processed']),
                'icon': If(Eval('state') == 'cancelled',
                    'tryton-undo',
                    'tryton-back'),
                'depends': ['state'],
                },
            'process': {
                'invisible': Eval('state') != 'draft',
                'icon': 'tryton-forward',
                'depends': ['state'],
                },
            'post': {
                'invisible': Eval('state') != 'processed',
                'icon': 'tryton-forward',
                'depends': ['state'],
                },
            'pay': {
                'invisible': Eval('state') != 'posted',
                'icon': 'tryton-ok',
                'depends': ['state'],
            },
            'refresh': {
                'invisible': Eval('state') == 'draft',
                'icon': 'tryton-refresh',
                'depends': ['state'],
                },
            'calculate': {
                'invisible': Eval('state') != 'draft',
                'icon': 'tryton-refresh',
                'depends': ['state'],
                },
            })

    @staticmethod
    def default_account_debit():
        pool = Pool()
        Configuration = pool.get('loan_customer.configuration')
        configuration = Configuration(1)
        company = Transaction().context.get('company')
        account_debit = configuration.get_multivalue(
                    'default_account_debit', company=company)
        if account_debit:
            return account_debit.id

    def get_principal_balance(self, name):
        return sum(ln.capital_pending for ln in self.lines)

    @classmethod
    def get_value_field(cls, records, names=None):
        default = {
            'pending_amount': Decimal(0),
            'due_date': None,
            'last_paid': 0,
            'due_days': 0,
            'fee_paid': 0,
            'fee_value': 0,
            'fee_value_overdue': 0,
            'fee_quantity_overdue': 0,
            'fee_overdue': 0,
            'total_fees': 0,
            'last_maturity': None,
            }
        result = {}
        ids = [a.id for a in records]
        for name in list(default.keys()):
            result[name] = dict((i, default[name]) for i in ids)

        for record in records:
            if record.state == 'draft':
                continue
            record_id = record.id
            lines = [l for l in record.lines if l.state == 'pending']
            last_paid = [l for l in record.lines if l.state == 'paid']
            lines_overdue = [l for l in record.lines if l.days_of_delay and l.days_of_delay < 0]
            quantity_overdue = len(lines_overdue)
            line = None
            pending_amount = 0
            if lines:
                line = lines[0]
                pending_amount = line.get_value(lines, ['capital_pending', 'interest_pending'])
                pending_amount = sum(pending_amount['capital_pending'].values()) + sum(pending_amount['interest_pending'].values())
            quanty_lines = len(lines)
            # result['principal_balance'][record_id] = sum(pending_amount['capital_pending'].values())
            result['pending_amount'][record_id] = pending_amount
            if lines or lines_overdue:
                result['due_date'][record_id] = lines[quantity_overdue].maturity_date if quanty_lines > quantity_overdue else lines_overdue[-1].maturity_date
            else:
                result['due_date'][record_id] = last_paid[-1].maturity_date
            result['due_days'][record_id] = line.days_of_delay if line else 0
            result['fee_value_overdue'][record_id] = sum(l.capital_pending + l.interest_pending for l in lines_overdue)
            result['fee_quantity_overdue'][record_id] = quantity_overdue
            result['total_fees'][record_id] = len(record.lines)
            result['fee_paid'][record_id] = last_paid[-1].sequence if last_paid else 0
            result['fee_value'][record_id] = line.total_amount if line else 0
            result['last_paid'][record_id] = last_paid[-1].maturity_date if last_paid else None
            result['last_maturity'][record_id] = list(record.lines)[-1].maturity_date

        for name in list(default.keys()):
            if name not in names:
                del result[name]

        return result

    @classmethod
    @ModelView.button
    @Workflow.transition('processed')
    @set_employee('process_by')
    def process(cls, disbursements):
        for disbursement in disbursements:
            if not disbursement.lines:
                continue
            disbursement.check_disbursement()
            # if not loan.check_loan_approve():
            #     raise AccessError(gettext('loan.msg_error_loan_payment_plan'))
            if not disbursement.number:
                disbursement.set_number()
            DisbursementLine = Pool().get('loan_disbursement.line')
            DisbursementLine.write(list(disbursement.lines), {'state': 'pending'})

    def check_disbursement(self):
        pool = Pool()
        Config = pool.get('loan_customer.configuration')
        config = Config(1)
        capital = sum(ln.capital for ln in self.lines)
        if config.require_invoice and not self.loan.credit_migrated and not self.invoice:
            raise AccessError(gettext('loan.msg_require_invoice'))
        if self.loan.amount < self.amount:
            raise AccessError(gettext('loan.msg_loan_amount_less_than_disbursement_amount'))
        if not self.loan.credit_migrated and capital != self.amount:
            raise AccessError(gettext('loan.msg_lines_plan_diferent_amount'))
        if self.loan.credit_migrated and capital != self.loan.historical_balance:
            raise AccessError(gettext('loan.msg_amount_diferent_historical_balance'))
        elif self.invoice and self.invoice.amount_to_pay != capital:
            raise AccessError(gettext('loan.msg_plan_lines_diferent_amount_to_pay'))

    @classmethod
    @ModelView.button
    def calculate(cls, records):
        for record in records:
            if record.payment_term and record.amount and record.currency:
                if record.lines:
                    pool = Pool()
                    DisbursementLine = pool.get('loan_disbursement.line')
                    DisbursementLine.delete(record.lines)
                record.calculate_loan()

    @classmethod
    @ModelView.button_action('loan.wizard_pay_loan')
    def pay(cls, records):
        pass

    @classmethod
    @ModelView.button
    def refresh(cls, disbursements):
        paid = []
        posted = []
        # cls.validate_disbursement(disbursements)
        for disbursement in disbursements:
            if disbursement.state not in ('posted', 'paid'):
                continue
            if disbursement.reconciled:
                paid.append(disbursement)
            else:
                posted.append(disbursement)
        cls.paid(paid)
        cls.post(posted)

    @classmethod
    @Workflow.transition('draft')
    def draft(cls, disbursements):
        pass

    @classmethod
    @Workflow.transition('paid')
    def paid(cls, disbursements):
        Date = Pool().get('ir.date')
        # cls._clean_payments(disbursements)
        cls.write(disbursements, {'closed_date': Date.today()})

    # @classmethod
    # def _clean_payments(cls, disbursements):
    #     to_write = []
    #     for disbursement in disbursements:
    #         to_remove = []
    #         reconciliations = [l.reconciliation for l in disbursement.lines_to_pay]
    #         for payment_line in disbursement.payment_lines:
    #             if payment_line.reconciliation not in reconciliations:
    #                 to_remove.append(payment_line.id)
    #         if to_remove:
    #             to_write.append([disbursement])
    #             to_write.append({
    #                 'payment_lines': [('remove', to_remove)],
    #             })
    #     if to_write:
    #         cls.write(*to_write)

    def get_reconciled(self, name):
        paid = True
        for ln in self.lines:
            if ln.state != 'paid':
                paid = False
                break
        return paid
        # def get_reconciliation(line):
        #     if line.reconciliation and line.reconciliation.delegate_to:
        #         return get_reconciliation(line.reconciliation.delegate_to)
        #     else:
        #         return line.reconciliation
        # reconciliations = list(map(get_reconciliation, self.lines_to_pay))
        # if not reconciliations:
        #     return None
        # elif not all(reconciliations):
        #     return None
        # else:
        #     return max(r.date for r in reconciliations)

    @fields.depends('payday', 'date_effective')
    def on_change_payday(self, name=None):
        if self.payday and self.date_effective:
            try:
                date(self.date_effective.year, self.date_effective.month, self.payday)
            except:
                raise AccessError(gettext('loan.msg_error_payday'))

    def calculate_loan(self):
        pool = Pool()
        DisbursementLine = pool.get('loan_disbursement.line')
        lines_to_create = []
        k = 0
        tmp_balance = 0
        if not self.loan.annual_effective_rate:
            tmp_balance = self.amount
        date_ = date(self.date_effective.year, self.date_effective.month, self.payday)
        for date_compute, amount in self.payment_term.compute(
                self.amount, self.currency, date_):
            tmp_balance = rvalue(tmp_balance - amount)
            k += 1
            lines_to_create.append({
                    'sequence': k,
                    'disbursement': self.id,
                    'maturity_date': date_compute,
                    'capital': amount,
                    'interest': 0,
                    'total_amount': amount,
                    'balance': tmp_balance,
                    'state': 'draft',
                    })
        if self.loan.annual_effective_rate:
            tmp_balance = self.amount
            tem = ConversionRate.tea_to_tem(self.loan.annual_effective_rate).quantize(Decimal(1) / 10 ** 4)
            amount_due = ConversionRate.get_amount_due(tem, tmp_balance, len(self.payment_term.lines))
            amount_due = amount_due
            sum_capital = 0
            for k, l in enumerate(lines_to_create):
                interest = tmp_balance * tem
                capital = rvalue(amount_due - interest)
                tmp_balance = rvalue(tmp_balance - capital)
                if k + 1 == len(lines_to_create):
                    capital += tmp_balance
                    tmp_balance -= tmp_balance
                l['capital'] = capital
                l['interest'] = rvalue(interest)
                l['total_amount'] = rvalue(amount_due)
                l['balance'] = tmp_balance
                sum_capital += capital
        next_fee = self.loan.next_fee
        historical_balance = self.loan.historical_balance
        if next_fee:
            lines = []
            for ln in lines_to_create:
                if ln['sequence'] < next_fee:
                    continue
                lines.append(ln)
            lines_to_create = lines
            total_amount = sum(ln['capital'] for ln in lines)
            if total_amount > historical_balance:
                amount = total_amount - historical_balance
                lines[0]['capital'] = lines[0]['capital'] - amount
                lines[0]['total_amount'] = lines[0]['interest'] + lines[0]['capital']
        DisbursementLine.create(lines_to_create)

    # @classmethod
    # def add_payment_lines(cls, payments):
    #     "Add value lines to the key loan from the payment dictionary."
    #     to_write = []
    #     for disbursement, lines in payments.items():
    #         if disbursement.state == 'paid':
    #             raise AccessError(
    #                 gettext('loan'
    #                     '.msg_loan_payment_lines_add_remove_paid',
    #                     loan=disbursement.rec_name))
    #         to_write.append([disbursement])
    #         to_write.append({'payment_lines': [('add', lines)]})
    #     if to_write:
    #         cls.write(*to_write)

    # @classmethod
    # def remove_payment_lines(cls, lines):
    #     "Remove payment lines from their disbursement."
    #     pool = Pool()
    #     PaymentLine = pool.get('loan-account.move.line')

    #     payments = defaultdict(list)
    #     ids = list(map(int, lines))
    #     for sub_ids in grouped_slice(ids):
    #         payment_lines = PaymentLine.search([
    #                 ('line', 'in', list(sub_ids)),
    #                 ])
    #         for payment_line in payment_lines:
    #             payments[payment_line.loan].append(payment_line.line)

    #     to_write = []
    #     for loan, lines in payments.items():
    #         if loan.state == 'paid':
    #             raise AccessError(
    #                 gettext('loan'
    #                     '.msg_loan_payment_lines_add_remove_paid',
    #                     loan=loan.rec_name))
    #         to_write.append([loan])
    #         to_write.append({'payment_lines': [('remove', lines)]})
    #     if to_write:
    #         cls.write(*to_write)

    # @classmethod
    # def get_amount_to_pay(cls, loans, name):
    #     pool = Pool()
    #     Currency = pool.get('currency.currency')
    #     Date = pool.get('ir.date')

    #     today = Date.today()
    #     res = dict((x.id, Decimal(0)) for x in loans)
    #     for loan in loans:
    #         if loan.state != 'posted':
    #             continue
    #         amount = Decimal(0)
    #         amount_currency = Decimal(0)
    #         for line in loan.lines_to_pay:
    #             if line.reconciliation:
    #                 continue
    #             # if (name == 'amount_to_pay_today'
    #             #         and (not line.maturity_date
    #             #             or line.maturity_date > today)):
    #             #     continue
    #             # else:
    #             amount += line.debit - line.credit
    #         for line in loan.payment_lines:
    #             if line.reconciliation:
    #                 continue
    #             # if (line.second_currency
    #             #         and line.second_currency == loan.currency):
    #             #     amount_currency += line.amount_second_currency
    #             # else:
    #             amount += line.debit - line.credit
    #         if amount != Decimal(0):
    #             pass
    #             # with Transaction().set_context(date=loan.currency_date):
    #             #     amount_currency += Currency.compute(
    #             #         loan.company.currency, amount, loan.currency)
    #         res[loan.id] = amount
    #     return res

    @classmethod
    def get_lines_to_pay(cls, disbursements, name):
        pool = Pool()
        MoveLine = pool.get('account.move.line')
        line = MoveLine.__table__()
        disbursement = cls.__table__()
        cursor = Transaction().connection.cursor()

        lines = defaultdict(list)
        for sub_ids in grouped_slice(disbursements):
            red_sql = reduce_ids(disbursement.id, sub_ids)
            cursor.execute(*disbursement.join(line,
                condition=((disbursement.account_move == line.move)
                    & (disbursement.account_debit == line.account))).select(
                        disbursement.id, line.id,
                        where=red_sql,
                        order_by=(disbursement.id, line.maturity_date.nulls_last)))
            for disbursement_id, line_id in cursor:
                lines[disbursement_id].append(line_id)
        return lines

    @classmethod
    @ModelView.button
    @Workflow.transition('posted')
    def post(cls, disbursements):
        cls._post(disbursements)

    def set_number(self):
        pool = Pool()
        Configuration = pool.get('loan_customer.configuration')
        configuration = Configuration(1)
        company = Transaction().context.get('company')
        sequence = configuration.get_multivalue('disbursement_sequence',
                    company=company)
        if not sequence:
            raise LoanMissingSequence(gettext('loan.msg_sequence_missing'))
        seq = sequence.get()
        self.write([self], {'number': seq})

    @classmethod
    def _post(cls, disbursements):
        pool = Pool()
        Move = pool.get('account.move')
        MoveLine = pool.get('account.move.line')
        for disbursement in disbursements:
            if disbursement.account_move:
                continue
            move = disbursement.get_move()
            Move.save([move])
            disbursement.account_move = move
            disbursement.state = 'posted'
            cls.save([disbursement])
            Move.post([move])
            # asdfasdf
            to_reconciled = []
            line_move = disbursement.loan.line_move
            if disbursement.invoice or line_move:
                account_id = disbursement.payment_mode.account.id
                for ln in move.lines:
                    if ln.account.id == account_id and ln.credit > 0:
                        to_reconciled.append(ln)
                if disbursement.invoice:
                    for ln in disbursement.invoice.move.lines:
                        if ln.account.id == account_id:
                            to_reconciled.append(ln)
                    to_reconciled.extend(disbursement.invoice.payment_lines)
                elif line_move:
                    to_reconciled.append(line_move)

            if to_reconciled:
                MoveLine.reconcile(to_reconciled)

    def get_move(self):
        pool = Pool()
        Move = pool.get('account.move')
        Period = pool.get('account.period')
        MoveLine = pool.get('account.move.line')
        # Date = pool.get('ir.date')

        account_id = self.payment_mode.account.id
        if self.invoice and self.invoice.account.id != account_id:
            raise AccessError(gettext('loan.msg_account_invoice'))
        elif self.loan.line_move and self.loan.line_move.account.id != account_id:
            raise AccessError(gettext('loan.msg_account_line_move'))
        period_id = Period.find(self.company.id, date=self.date_effective)
        move_lines = []
        total = Decimal('0.0')
        for line in self.lines:
            move_lines += line.get_move_lines()
            total += line.capital
        line = MoveLine()
        line.amount_second_currency = None
        line.second_currency = None
        line.debit, line.credit = 0, total
        line.account = account_id
        if line.account.party_required:
            line.party = self.party.id
        line.description = self.description
        move_lines += [line]

        move = Move()
        move.journal = self.payment_mode.journal.id
        move.period = period_id
        move.date = self.date_effective
        move.origin = self
        move.company = self.company
        move.lines = move_lines
        return move

    def call_overdue_amount(self):
        Date = Pool().get('ir.date')
        today = Date.today()
        amount = sum([l.capital_pending + l.interest_pending + l.interest_delay_pending for l in self.lines if l.maturity_date <= today and l.state in ('pending', 'partial')])
        return amount

    def apply_payment(self, pay_date, fees, description, amount_to_pay, payment_mode):
        pool = Pool()
        Voucher = pool.get('account.voucher')
        MoveLine = pool.get('account.move.line')
        Configuration = pool.get('loan_customer.configuration')
        DisbursementLine = pool.get('loan_disbursement.line')
        config = Configuration(1)
        account_id = Voucher.get_account('receipt', payment_mode)
        party_id = self.party.id
        voucher_to_create = {
            'party': party_id,
            'voucher_type': 'receipt',
            'date': pay_date,
            'description': description,
            'payment_mode': payment_mode.id,
            'state': 'draft',
            'account': account_id,
            'journal': payment_mode.journal.id,
            'lines': [('create', [])],
            'method_counterpart': 'one_line',
            'amount_to_pay': amount_to_pay,
        }

        default_values = {
            'interest_delay': config.default_account_interest_delay,
            'interest': config.default_account_interest,
            'capital': self.account_debit,
        }
        if fees and amount_to_pay:
            account_fee = config.default_account_fees
            amount_to_pay -= fees

            voucher_to_create['lines'][0][1].append({
                        'detail': account_fee.name,
                        'amount': fees,
                        'account': account_fee.id,
                        'party': party_id,
                    })
        lines_to_create = []
        lines_validate = []
        for line in self.lines:
            if line.state not in ('pending', 'partial'):
                continue

            if amount_to_pay <= 0:
                break
            line_id = line.id
            line_ = {'disbursement_line': line_id, 'disbursement': line.disbursement, 'effective_date': pay_date}
            lines_validate.append(line)
            for k, v in default_values.items():
                print(k, getattr(line, k + '_pending'), amount_to_pay)
                if amount_to_pay <= 0:
                    break

                if amount_to_pay >= getattr(line, k + '_pending'):
                    amount = rvalue(getattr(line, k + '_pending'))
                    value = {
                        'detail': v.name,
                        'amount': amount,
                        'account': v.id,
                        'party': party_id,
                    }
                    line_[k] = amount
                    amount_to_pay -= amount
                elif amount_to_pay < getattr(line, k + '_pending') and amount_to_pay > 0:
                    amount = amount_to_pay
                    value = {
                        'detail': v.name,
                        'amount': amount,
                        'account': v.id,
                        'party': party_id,
                    }
                    line_[k] = amount
                    amount_to_pay -= amount

                if k == 'capital':
                    move_lines = MoveLine.search(['origin', '=', 'loan_disbursement.line,' + str(line_id)])
                    value['move_line'] = move_lines[0] if move_lines else None

                voucher_to_create['lines'][0][1].append(value)
            lines_to_create.append(line_)
        vouchers = Voucher.create([voucher_to_create])
        Voucher.post(vouchers)
        if lines_to_create:
            for line_ in lines_to_create:
                line_['voucher'] = vouchers[0]
            PaymentLine = Pool().get('loan_payment.line')
            PaymentLine.create(lines_to_create)
        DisbursementLine.validate_line(lines_validate)


class DisbursementLine(ModelSQL, ModelView):
    "Disbursement Line"
    __name__ = 'loan_disbursement.line'

    sequence = fields.Char('Sequence')
    disbursement = fields.Many2One('loan.disbursement', 'Disbursement',
        ondelete='CASCADE', required=True)
    capital = fields.Numeric('Capital', digits=(16, 2))
    interest = fields.Numeric('Interest', digits=(16, 2))
    total_amount = fields.Numeric('Total Amount', digits=(16, 2))
    balance = fields.Numeric('Balance', digits=(16, 2))
    maturity_date = fields.Date('Maturity Date',
        required=True)
    state = fields.Selection([
            ('draft', 'Draft'),
            ('pending', 'Pending'),
            ('partial', 'Partial'),
            ('paid', 'Paid'),
        ], 'State', states={
            'required': True,
        }, depends=['origin'])
    description = fields.Char('Description')
    currency = fields.Function(fields.Many2One(
            'currency.currency', "Currency"), 'on_change_with_currency')

    origin = fields.Reference("Origin", selection='get_origin')
    days_of_delay = fields.Function(fields.Integer('Days of Delay'), 'get_days_of_delay')
    interest_delay = fields.Function(fields.Numeric('Interest Delay', digits=(16, 2)), 'get_interest_delay')
    payment_line = fields.One2Many('loan_payment.line', 'disbursement_line', 'Payment Line')
    capital_pending = fields.Function(fields.Numeric('Capital Pending'), 'get_value')
    interest_pending = fields.Function(fields.Numeric('Interest Pending'), 'get_value')
    interest_delay_pending = fields.Function(fields.Numeric('Interest Delay Pending'), 'get_value')
    value_pending = fields.Function(fields.Numeric('Value Pending', digits=(16, 2)), 'get_value_pending')

    @classmethod
    def __setup__(cls):
        super(DisbursementLine, cls).__setup__()
        cls._order.insert(0, ('maturity_date', 'ASC'))

    @staticmethod
    def default_state():
        return 'draft'

    @fields.depends('disbursement', '_parent_disbursement.currency')
    def on_change_with_currency(self, name=None):
        if self.disbursement:
            return self.disbursement.currency.id

    def get_value_pending(self, name=None):
        amount = sum((self.capital_pending, self.interest_pending, self.interest_delay_pending))
        return amount.quantize(Decimal(1) / 10 ** 2)

    @classmethod
    def get_value(cls, lines, names):
        result = {
            'capital_pending': {},
            'interest_pending': {},
            'interest_delay_pending': {},
        }

        for line in lines:
            capital_pending = line.capital or 0
            interest_pending = line.interest or 0
            interest_delay_pending = line.interest_delay or 0
            for p in line.payment_line:
                capital_pending -= p.capital or 0
                interest_pending -= p.interest or 0
                # interest_delay_pending -= p.interest_delay or 0

            result['capital_pending'][line.id] = Decimal(capital_pending).quantize(Decimal(1) / 10 ** 2)
            result['interest_pending'][line.id] = Decimal(interest_pending).quantize(Decimal(1) / 10 ** 2)
            result['interest_delay_pending'][line.id] = Decimal(interest_delay_pending).quantize(Decimal(1) / 10 ** 2)

        for key in list(result.keys()):
            if key not in names:
                del result[key]
        return result

    def get_move_lines(self):
        pool = Pool()
        MoveLine = pool.get('account.move.line')
        line = MoveLine()
        capital = self.capital
        line.amount_second_currency = None
        line.second_currency = None
        line.debit, line.credit = capital, 0
        line.account = self.disbursement.account_debit
        if line.account.party_required:
            line.party = self.disbursement.party.id
        line.description = self.disbursement.description
        line.origin = self
        line.maturity_date = self.maturity_date
        return [line]

    @classmethod
    def _get_origin(cls):
        "Return list of Model names for origin Reference"
        return ['loan_disbursement.line']

    @classmethod
    def get_origin(cls):
        Model = Pool().get('ir.model')
        get_name = Model.get_name
        models = cls._get_origin()
        return [(None, '')] + [(m, get_name(m)) for m in models]

    @classmethod
    def get_days_of_delay(cls, lines, name, cutoff_date=None):
        days_delay = {}
        Date = Pool().get('ir.date')
        end_date = Date.today()
        context = Transaction().context
        if cutoff_date:
            end_date = cutoff_date
        for line in lines:
            start_date = line.maturity_date
            if line.payment_line and context.get('validate_interest_delay'):
                start_date = max([l.effective_date for l in line.payment_line])
            delay = None
            if line.state in ('pending', 'partial') and end_date > line.maturity_date:
                delay = (start_date - end_date).days
            days_delay[line.id] = delay
        return days_delay

    @classmethod
    def get_interest_delay(cls, lines, name, cutoff_date=None):
        interest_delay = {}
        context = Transaction().context
        if context.get('cutoff_date'):
            cutoff_date = context['cutoff_date']

        context['validate_interest_delay'] = True
        with Transaction().set_context(context):
            for line in lines:
                interest = 0
                type_loan = line.disbursement.loan.credit_line.type_loan
                rate_delay = cls.get_rate_delay(type_loan)
                capital_ = line.capital - sum([p.capital for p in line.payment_line if p.capital])
                if not cutoff_date and rate_delay and capital_ > 0 and line.days_of_delay:
                    interest = abs((capital_ * line.days_of_delay * rate_delay / 100) / 30)
                elif cutoff_date and rate_delay and capital_ > 0:
                    days_of_delay = cls.get_days_of_delay([line], name, cutoff_date)
                    interest = abs((capital_ * days_of_delay[line.id] * rate_delay / 100) / 30) if days_of_delay[line.id] else 0
                interest_delay[line.id] = Decimal(interest).quantize(Decimal(1) / 10 ** 2)
        return interest_delay

    @classmethod
    def get_rate_delay(cls, type_loan, date_delay=None):
        tem = None
        if not date_delay:
            Date = Pool().get('ir.date')
            date_delay = Date.today()
        History_rate = Pool().get('history.rate')
        history_rates = History_rate.search([
                ('type_loan', '=', type_loan),
                ('start_date', '<=', date_delay),
                ('end_date', '>=', date_delay),
            ])
        if history_rates:
            tem = history_rates[0].month_effective_rate
        return tem

    @classmethod
    def write(cls, *args):
        super(DisbursementLine, cls).write(*args)
        actions = iter(args)
        all_lines = []
        for lines, values in zip(actions, actions, strict=False):
            all_lines.extend(lines)
        cls.validate_line(all_lines)

    @classmethod
    def validate_line(cls, lines):
        line = cls.__table__()
        cursor = Transaction().connection.cursor()
        pending_lines = []

        paid_lines = []
        for m in lines:
            if (m.capital_pending <= 0 and m.state != 'paid') or (m.capital_pending > 0 and m.state == 'paid'):
                paid_lines.append(m.id)
        for line_ids, state in (
                (pending_lines, 'pending'),
                (paid_lines, 'paid'),
                ):
            if line_ids:
                for sub_ids in grouped_slice(line_ids):
                    red_sql = reduce_ids(line.id, sub_ids)
                    # Use SQL to prevent double validate loop
                    cursor.execute(*line.update(
                            columns=[line.state],
                            values=[state],
                            where=red_sql))


class DisbursementReport(CompanyReport):
    __name__ = 'loan.disbusement.report'

    @classmethod
    def execute(cls, ids, data):
        with Transaction().set_context(address_with_party=True):
            return super(DisbursementReport, cls).execute(ids, data)

    @classmethod
    def get_context(cls, records, header, data):
        pool = Pool()
        Date = pool.get('ir.date')
        context = super().get_context(records, header, data)
        context['today'] = Date.today()
        return context


class LoanPaymentLine(ModelSQL, ModelView):
    "Loan - Payment Line"
    __name__ = 'loan_payment.line'

    disbursement = fields.Many2One('loan.disbursement', 'Loan Disbursement')
    disbursement_line = fields.Many2One('loan_disbursement.line', 'Loan Disbursement Line', required=True)
    effective_date = fields.Date('Effective Date')
    capital = fields.Numeric('Capital', digits=(16, 2))
    interest = fields.Numeric('Interest', digits=(16, 2))
    interest_delay = fields.Numeric('Interest Delay', digits=(16, 2))
    # fees = fields.Numeric('Fees', digits=(16,2))
    days_delay = fields.Function(fields.Integer('Days Delay'), 'get_days_delay')
    total_amount = fields.Function(fields.Numeric('Total Amount', digits=(16, 2)), 'get_total_amount')
    voucher = fields.Many2One('account.voucher', 'Voucher')

    @classmethod
    def __setup__(cls):
        super(LoanPaymentLine, cls).__setup__()
        # t = cls.__table__()

    @classmethod
    def get_total_amount(cls, records, names):
        result = {}
        for name in names:
            result[name] = {}
        fields_names = ('capital', 'interest', 'interest_delay')
        for record in records:
            amount = sum([getattr(record, f) for f in fields_names if getattr(record, f)])
            result['total_amount'][record.id] = amount
        return result

    def get_days_delay(self, name):
        if self.disbursement_line and self.effective_date:
            return (self.disbursement_line.maturity_date - self.effective_date).days


class PayLoan(Wizard):
    "Pay Loan"
    __name__ = 'loan.disbursement.pay_loan'

    start = StateView('loan.disbursement.pay_loan.start',
        'loan.pay_loan_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Pay', 'pay', 'tryton-ok', default=True),
            ])
    pay = StateTransition()

    def default_start(self, fields):
        return {
            'company': self.record.company.id,
            'overdue_amount': self.record.call_overdue_amount(),
            }

    def transition_pay(self):
        Date = Pool().get('ir.date')
        today = Date.today()
        context = Transaction().context
        if self.start.pay_date != today:
            context['cutoff_date'] = self.start.pay_date

        with Transaction().set_context(context):
            self.record.apply_payment(
                self.start.pay_date,
                self.start.fees,
                self.start.description,
                self.start.amount_to_pay,
                self.start.payment_mode,
                )
        return 'end'


class PayLoanStart(ModelView):
    "Pay Loan"
    __name__ = 'loan.disbursement.pay_loan.start'
    company = fields.Many2One('company.company', 'Company', readonly=True)
    overdue_amount = fields.Numeric('Overdue Amount', digits=(16, 2), readonly=True)
    pay_date = fields.Date('Pay Date', help="Date of customer's voucher")
    fees = fields.Numeric('Fees', digits=(16, 2))
    amount_to_pay = fields.Numeric('Amount to pay', digits=(16, 2))
    description = fields.Char('Description', required=True)
    payment_mode = fields.Many2One('account.voucher.paymode', 'Payment Mode')

    @classmethod
    def default_pay_date(cls):
        Date = Pool().get('ir.date')
        today = Date.today()
        return today


class LoanDetailedStart(ModelSQL, ModelView):
    "Loan Detailed Start"
    __name__ = 'loan_detailed.start'

    company = fields.Many2One('company.company', 'Company', required=True)
    start_date = fields.Date('Start Date')
    end_date = fields.Date('End Date', required=True)

    @staticmethod
    def default_company():
        return Transaction().context.get('company')

    @staticmethod
    def default_shop():
        return Transaction().context.get('shop')

    @staticmethod
    def default_end_date():
        Date = Pool().get('ir.date')
        return Date.today()

    @staticmethod
    def default_start_date():
        Date = Pool().get('ir.date')
        today = Date.today()
        date_ = date(today.year, today.month, 1)
        return date_


class LoanDetailedWizard(Wizard):
    "Loan Detailed Wizard"
    __name__ = 'loan_detailed.wizard'

    start = StateView('loan_detailed.start',
        'loan.loan_detailed_start_view_form', [
            Button('Cancel', 'end', 'tryton-cancel'),
            Button('Print', 'print_', 'tryton-ok', default=True),
            ])
    print_ = StateReport('report.loan_detailed')

    def do_print_(self, action):
        report_context = {
            'company': self.start.company.id,
            'start_date': self.start.start_date,
            'end_date': self.start.end_date,
        }
        return action, report_context

    def transition_print_(self):
        return 'end'


class ReportLoanDetailed(Report):
    "Report Loan Detailed"
    __name__ = "report.loan_detailed"

    @classmethod
    def get_context(cls, records, header, data):
        report_context = super().get_context(records, header, data)
        pool = Pool()
        Disbursement = pool.get('loan.disbursement')
        Company = pool.get('company.company')

        records = Disbursement.search([
            ('state', 'in', ['posted', 'processed']),
            ('date_effective', '<=', data['end_date']),
            ])
        records_closed = Disbursement.search([
            ('state', '=', 'paid'),
            ('closed_date', '>=', data['start_date']),
            ])
        fields = [
            'pending_amount', 'due_date', 'due_days', 'fee_paid',
            'fee_value', 'last_paid', 'fee_value_overdue',
            'fee_quantity_overdue', 'fee_overdue', 'total_fees',
            'last_maturity',
            ]

        values = Disbursement.get_value_field(records, fields)
        values_closed = Disbursement.get_value_field(records_closed, fields)
        responsible = {
            'ppal': '00',  # Principal
            'co-sign': '01',  # Codeudor
            'avalista': '04',  # Avalista
            'debtor': '05',  # Deudor Solidario
            'co-tenant': '06',  # Co-arrendatario
            'other': '07',  # otro
            'surety': '08',  # fiador
            }
        document = {
            '13': 1, '31': 2, '50': 3, '22': 4, '41': 5,
            '42': 6, '12': 7, '91': 8, '47': 9}
        print(records, 'validate records')
        report_context['records'] = records
        report_context['values'] = values
        report_context['records_closed'] = records_closed
        report_context['values_closed'] = values_closed
        report_context['company'] = Company(data['company'])
        report_context['document_type'] = document
        report_context['responsible'] = responsible
        report_context['age_portfolio'] = cls.get_age_portfolio
        return report_context

    @classmethod
    def get_age_portfolio(cls, value):
        if not value:
            return '1'
        value_ = abs(value)
        result = '1'
        if value_ > 30 and value_ <= 60:
            result = '6'
        elif value_ > 60 and value_ <= 90:
            result = '7'
        elif value_ > 90 and value_ <= 120:
            result = '8'
        elif value_ > 120:
            result = '9'
        return result
